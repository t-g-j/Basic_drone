#!/usr/bin/env python

from math import sqrt


def pathReductionPercentageLength(northern, eastern, percentage):
	# percentage = minimum limit of the length of the path after reduction. (example : 70 -> The shortened path will not be shorter than 70% of the initial path

	initialLength = 0

	# Calculate the target final length of the path
	for i in range(0, len(northern) - 1):
		initialLength = initialLength + sqrt((northern[i] - northern[i + 1]) ** 2 + (eastern[i] - eastern[i + 1]) ** 2)
	finallength = initialLength * percentage / 100

	print("initial length")
	print(initialLength)
	print("trget length")
	print(finallength)
	# init
	length = initialLength
	newnorthern = northern
	neweastern = eastern

	while length >= finallength and len(newnorthern) >= 2:

		oldnorthern = newnorthern
		oldeastern = neweastern
		if len(newnorthern) == 2:
			break

		# calculate the area of the first triangle to init areamin
		a = sqrt((oldnorthern[1] - oldnorthern[0]) ** 2 + (neweastern[1] - neweastern[0]) ** 2)
		b = sqrt((oldnorthern[2] - oldnorthern[0]) ** 2 + (neweastern[2] - neweastern[0]) ** 2)
		c = sqrt((oldnorthern[2] - oldnorthern[1]) ** 2 + (neweastern[2] - neweastern[1]) ** 2)
		hPer = (a + b + c) / 2
		areamin = sqrt(hPer * (hPer - a) * (hPer - b) * (hPer - c))
		imin = 1

		# calculate al the triangle areas nd keep the index of the smallest one
		for i in range(2, (len(oldnorthern) - 2)):
			a = sqrt((oldnorthern[i] - oldnorthern[i - 1]) ** 2 + (neweastern[i] - neweastern[i - 1]) ** 2)
			b = sqrt((oldnorthern[i + 1] - oldnorthern[i - 1]) ** 2 + (neweastern[i + 1] - neweastern[i - 1]) ** 2)
			c = sqrt((oldnorthern[i + 1] - oldnorthern[i]) ** 2 + (neweastern[i + 1] - neweastern[i]) ** 2)
			hPer = (a + b + c) / 2
			area = sqrt(hPer * (hPer - a) * (hPer - b) * (hPer - c))
			if area < areamin:
				areamin = area
				imin = i

		# remove the dot of the smallest triangle
		newnorthern = oldnorthern[:]
		newnorthern.pop(imin)
		neweastern = oldeastern[:]
		neweastern.pop(imin)

		# calculate the the length of the path
		oldlength = length
		length = 0
		for i in range(0, len(newnorthern) - 1):
			length = length + sqrt(
				(newnorthern[i] - newnorthern[i + 1]) ** 2 + (neweastern[i] - neweastern[i + 1]) ** 2)

	length = 0
	for i in range(0, len(oldnorthern) - 1):
		length = length + sqrt((oldnorthern[i] - oldnorthern[i + 1]) ** 2 + (oldeastern[i] - oldeastern[i + 1]) ** 2)
	print("Final length")
	print(length)
	print("final length / initial length")
	print(length / initialLength)
	return [oldnorthern, oldeastern]